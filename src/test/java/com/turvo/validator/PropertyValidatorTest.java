package com.turvo.validator;

import com.turvo.configuration.exception.TurvoApplicationException;
import org.junit.Before;
import org.junit.Test;

public class PropertyValidatorTest {

    private PropertyValidator propertyValidator;

    @Before
    public void setUp() {
        propertyValidator = new PropertyValidator();
    }

    @Test
    public void validateDuplicatedPropertyValue_NotDuplicatedValue_Success() {
        propertyValidator.validateDuplicatedPropertyValue("Some property", "Value1", "Value2");
    }

    @Test(expected = TurvoApplicationException.class)
    public void validateDuplicatedPropertyValue_DuplicatedValue_Exception() {
        propertyValidator.validateDuplicatedPropertyValue("Some property", "Value1", "Value1");
    }

}
