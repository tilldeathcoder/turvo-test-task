drop table cities if exists;
drop table cities_distance if exists;
create table cities (id bigint not null, title varchar(255), primary key (id));
create table cities_distance (city1_id bigint not null, city2_id bigint not null, distance double, primary key (city1_id, city2_id));
alter table cities_distance add constraint FKjrfhfx39jy7oq1iyoxnc37ukv foreign key (city1_id) references cities;
alter table cities_distance add constraint FK46f2ebfvlgth1yx0xuaxugav6 foreign key (city2_id) references cities;