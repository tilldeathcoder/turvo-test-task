package com.turvo.service;

import com.turvo.service.dto.DistanceDto;

/**
 * The service class to work with {@link DistanceDto} objects.
 *
 * @author Yauhen.Makaranka
 * @see DistanceDto
 */
public interface DistanceService {

  /**
   * Create distance between two cities.
   *
   * @param distanceDto the distance object
   */
  void create(DistanceDto distanceDto);
}
