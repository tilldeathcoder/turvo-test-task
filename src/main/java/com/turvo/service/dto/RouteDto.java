package com.turvo.service.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * This class describes route between two cities.
 *
 * @author Yauhen.Makaranka
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RouteDto {
  private List<DistanceDto> distanceDtos;

  @Builder.Default private Double fullDistance = 0.0;
}
