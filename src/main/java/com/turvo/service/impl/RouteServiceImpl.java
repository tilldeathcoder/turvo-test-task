package com.turvo.service.impl;

import com.turvo.configuration.exception.TurvoApplicationException;
import com.turvo.configuration.exception.TurvoApplicationExceptionCodes;
import com.turvo.repository.DistanceRepository;
import com.turvo.service.dto.DistanceDto;
import com.turvo.service.RouteService;
import com.turvo.service.calculator.RouteCalculator;
import com.turvo.service.dto.RouteDto;
import com.turvo.validator.PropertyValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class RouteServiceImpl implements RouteService {

  private PropertyValidator propertyValidator;
  private DistanceRepository distanceRepository;

  @Autowired
  public RouteServiceImpl(
      PropertyValidator propertyValidator, DistanceRepository distanceRepository) {
    this.propertyValidator = propertyValidator;
    this.distanceRepository = distanceRepository;
  }

  @Override
  @Transactional
  public List<RouteDto> getRoutes(String start, String finish) {
    validateSearchQueryParameters(start, finish);
    List<DistanceDto> allDistances = distanceRepository.getAll().stream().map(DistanceDto::new).collect(Collectors.toList());

    RouteCalculator calculator = new RouteCalculator(allDistances, start, finish);
    List<RouteDto> routes = calculator.calculate();

    if (routes.isEmpty()) {
      throw new TurvoApplicationException(
          TurvoApplicationExceptionCodes.ROUTER_NOT_FOUND, start, finish);
    }

    return routes;
  }

  private void validateSearchQueryParameters(String start, String finish) {
    propertyValidator.validateDuplicatedPropertyValue("finish", start, finish);
    validateCityExist(start);
    validateCityExist(finish);
  }

  private void validateCityExist(String city) {
    if (distanceRepository.getByCity(city).isEmpty()) {
      throw new TurvoApplicationException(TurvoApplicationExceptionCodes.ENTITY_NOT_FOUND, city);
    }
  }
}
