package com.turvo.controller;

import com.turvo.service.dto.DistanceDto;
import com.turvo.service.dto.RouteDto;
import com.turvo.service.DistanceService;
import com.turvo.service.RouteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

/**
 * Rest controller to work with distance and route between cities.
 *
 * @author Yauhen.Makaranka
 * @see DistanceDto
 * @see RouteDto
 */
@RestController
@RequestMapping(value = "/cities", produces = "application/json")
public class CityController {

  private DistanceService distanceService;
  private RouteService routeService;

  @Autowired
  public CityController(DistanceService distanceService, RouteService routeService) {
    this.distanceService = distanceService;
    this.routeService = routeService;
  }

  /**
   * Create distance between two cities.
   *
   * @param distanceDto the distance object
   * @return the created distance or else you will get exception.
   */
  @PostMapping("/distance")
  public ResponseEntity<DistanceDto> create(@RequestBody @Valid DistanceDto distanceDto) {
    distanceService.create(distanceDto);

    return new ResponseEntity<>(distanceDto, HttpStatus.CREATED);
  }

  /**
   * Get route between two cities.
   *
   * @param start - the start point
   * @param finish - the finish point
   * @return the route between two cities
   */
  @GetMapping("/route/{start}/{finish}")
  public ResponseEntity<List<RouteDto>> getRoute(
      @PathVariable String start, @PathVariable String finish) {
    return new ResponseEntity<>(routeService.getRoutes(start, finish), HttpStatus.OK);
  }
}
