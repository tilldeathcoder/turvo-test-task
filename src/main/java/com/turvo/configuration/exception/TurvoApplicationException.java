package com.turvo.configuration.exception;

import lombok.Getter;

/**
 * This exception is thrown if any error occurs during application work.
 *
 * @author Yauhen.Makaranka
 */
@Getter
public class TurvoApplicationException extends RuntimeException {

  private final TurvoApplicationExceptionCodes info;
  private final Object[] params;

  public TurvoApplicationException(TurvoApplicationExceptionCodes info) {
    this.info = info;
    this.params = new Object[0];
  }

  public TurvoApplicationException(TurvoApplicationExceptionCodes info, Object... params) {
    this.info = info;
    this.params = params;
  }
}
