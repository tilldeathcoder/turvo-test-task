package com.turvo.configuration.database;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@EnableTransactionManagement
public class DatabaseConfiguration {
  @Value("${spring.datasource.driverClassName}")
  private String driver;

  @Value("${spring.datasource.password}")
  private String password;

  @Value("${spring.datasource.url}")
  private String url;

  @Value("${spring.datasource.username}")
  private String username;

  @Value("${spring.jpa.database-platform}")
  private String dialect;

  @Value("${hibernate.show_sql}")
  private String showSql;

  @Value("${hibernate.ddl-auto}")
  private String hbm2DdlAuto;

  @Value("${hibernate.packagesToScan}")
  private String packagesToScan;

  @Value("${hibernate.cache.use_second_level_cache}")
  private String useSecondLevelCache;

  @Value("${hibernate.cache.use_query_cache}")
  private String useQueryCache;

  @Value("${hibernate.cache.region.factory_class}")
  private String cacheClass;

  @Bean
  public DataSource dataSource() {
    DriverManagerDataSource dataSource = new DriverManagerDataSource();
    dataSource.setDriverClassName(driver);
    dataSource.setUrl(url);
    dataSource.setUsername(username);
    dataSource.setPassword(password);
    return dataSource;
  }

  @Bean
  public LocalSessionFactoryBean sessionFactory() {
    LocalSessionFactoryBean sessionFactory = new LocalSessionFactoryBean();
    sessionFactory.setDataSource(dataSource());
    sessionFactory.setPackagesToScan(packagesToScan);
    Properties hibernateProperties = new Properties();
    hibernateProperties.put("hibernate.dialect", dialect);
    hibernateProperties.put("hibernate.show_sql", showSql);
    hibernateProperties.put("hibernate.hbm2ddl.auto", hbm2DdlAuto);
    hibernateProperties.put("hibernate.cache.use_second_level_cache", useSecondLevelCache);
    hibernateProperties.put("hibernate.cache.use_query_cache", useQueryCache);
    hibernateProperties.put("hibernate.cache.region.factory_class", cacheClass);
    sessionFactory.setHibernateProperties(hibernateProperties);

    return sessionFactory;
  }

  @Bean
  public PlatformTransactionManager txManager(EntityManagerFactory entityManagerFactory) {
    JpaTransactionManager transactionManager = new JpaTransactionManager();
    transactionManager.setEntityManagerFactory(entityManagerFactory);
    return transactionManager;
  }
}
