package com.turvo.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "cities_distance")
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@NamedQueries({
  @NamedQuery(
      name = "findDistanceByCityId",
      query =
          "select d from Distance d where (d.city1 in(select c from City c where c.id = :id1) and d.city2 in(select c from City c where c.id = :id2)) or "
              + "d.city1 in(select c from City c where c.id = :id2) and d.city2 in(select c from City c where c.id = :id1)"),
  @NamedQuery(
      name = "findDistancesByCityTitle",
      query =
          "select d from Distance d where d.city1 in(select c from City c where lower(c.title) = :title) or d.city2 in(select c from City c where lower(c.title) = :title)")
})
public class Distance {

  @EmbeddedId private DistanceCompositeKey id;

  @ManyToOne
  @MapsId("id")
  @JoinColumn(name = "city1_id")
  private City city1;

  @ManyToOne
  @MapsId("id")
  @JoinColumn(name = "city2_id")
  private City city2;

  @Column private Double distance;

  public Distance(City city1, City city2) {
    this.id = DistanceCompositeKey.builder().city1Id(city1.getId()).city2Id(city2.getId()).build();
    this.city1 = city1;
    this.city2 = city2;
  }
}
