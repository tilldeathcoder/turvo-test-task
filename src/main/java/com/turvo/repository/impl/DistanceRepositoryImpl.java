package com.turvo.repository.impl;

import com.turvo.domain.City;
import com.turvo.domain.Distance;
import com.turvo.repository.DistanceRepository;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.hibernate.internal.util.collections.CollectionHelper.isNotEmpty;

/**
 * The implementation of {@link DistanceRepository}. Created only for DEVELOPMENT!
 *
 * @author Yauhen.Makaranka
 */
@Repository
@Profile("dev")
@Slf4j
public class DistanceRepositoryImpl implements DistanceRepository {

  private SessionFactory sessionFactory;

  @Autowired
  public DistanceRepositoryImpl(SessionFactory sessionFactory) {
    this.sessionFactory = sessionFactory;
  }

  @Override
  public List<Distance> getAll() {
    Session session = this.sessionFactory.getCurrentSession();
    Query<Distance> query = session.createQuery("from  Distance");
    query.setCacheable(true);
    List<Distance> distances = query.getResultList();

    return distances;
  }

  @Override
  public List<Distance> getByCity(String city) {
    Session session = this.sessionFactory.getCurrentSession();
    Query<Distance> query =
        session
            .createNamedQuery("findDistancesByCityTitle", Distance.class)
            .setParameter("title", city.toLowerCase());
    query.setCacheable(true);

    return query.getResultList();
  }

  @Override
  public void insert(Distance newDistance) {
    Session session = this.sessionFactory.getCurrentSession();
    City city1 = getOrCreateCity(newDistance.getCity1().getTitle());
    City city2 = getOrCreateCity(newDistance.getCity2().getTitle());

    Optional<Distance> distanceOpt = getDistanceForCities(city1.getId(), city2.getId());

    Distance distance = distanceOpt.orElse(new Distance(city1, city2));
    distance.setDistance(newDistance.getDistance());
    session.saveOrUpdate(distance);
  }

  private City getOrCreateCity(String title) {
    Session session = this.sessionFactory.getCurrentSession();
    Query<City> query =
        session
            .createNamedQuery("getCityByTitle", City.class)
            .setParameter("title", title.toLowerCase());
    query.setCacheable(true);

    List<City> cities = query.list();

    if (isNotEmpty(cities)) {
      return cities.get(0);
    }

    City city = City.builder().title(title).build();
    session.save(city);
    return city;
  }

  private Optional<Distance> getDistanceForCities(long city1Id, long city2Id) {
    Session session = this.sessionFactory.getCurrentSession();
    Query<Distance> query =
        session
            .createNamedQuery("findDistanceByCityId", Distance.class)
            .setParameter("id1", city1Id)
            .setParameter("id2", city2Id);
    query.setCacheable(true);

    List<Distance> distances = query.list();
    return isNotEmpty(distances) ? Optional.of(distances.get(0)) : Optional.empty();
  }
}
