package com.turvo.repository;

import com.turvo.domain.Distance;
import com.turvo.service.dto.DistanceDto;

import java.util.List;

/**
 * This class is used for CRUD operations to database for {@link Distance} objects.
 *
 * @author Yauhen.Makaranka
 * @see Distance
 */
public interface DistanceRepository {

  /**
   * Get all {@link Distance} objects from database.
   *
   * @return the {@link List} of {@link Distance} objects.
   */
  List<Distance> getAll();

  /**
   * Get all {@link Distance} objects from database that connected to passed parameter.
   *
   * @param city - this parameter is used to filter result.
   * @return the {@link List} of {@link Distance} objects.
   */
  List<Distance> getByCity(String city);

  /**
   * Insert {@link Distance} object into database. If such distance already exists, it will be
   * overwritten, but only distance value will be changed, cities order will not be changed.
   *
   * @param distanceDto - the inserted object.
   */
  void insert(Distance distanceDto);
}
