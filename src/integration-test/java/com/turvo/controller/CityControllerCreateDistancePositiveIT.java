package com.turvo.controller;

import com.turvo.service.dto.DistanceDto;
import io.restassured.http.ContentType;
import org.apache.http.HttpStatus;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

import static io.restassured.RestAssured.given;

public class CityControllerCreateDistancePositiveIT extends BaseCityControllerIT {

  private void postDistanceAndValidateRequestStatusCodeBeCreated(DistanceDto distanceDto) {
    given()
        .log()
        .all()
        .body(distanceDto)
        .contentType(ContentType.JSON)
        .when()
        .post(basePath + DISTANCE_PATH)
        .then()
        .log()
        .all()
        .statusCode(HttpStatus.SC_CREATED);
  }

  @Test
  public void createDistance_PropertyStartWithOnlyLetters_Success() {
    postDistanceAndValidateRequestStatusCodeBeCreated(
        DistanceDto.builder().city1("Start").city2("Finish").distance(4.0).build());
  }

  @Test
  public void createDistance_PropertyStartWithOnlyDigits_Success() {
    postDistanceAndValidateRequestStatusCodeBeCreated(
        DistanceDto.builder().city1("111").city2("Finish").distance(4.0).build());
  }

  @Test
  public void createDistance_PropertyFinishWithOnlyLeters_Success() {
    postDistanceAndValidateRequestStatusCodeBeCreated(
        DistanceDto.builder().city1("Start").city2("Finish").distance(4.0).build());
  }

  @Test
  public void createDistance_PropertyFinishWithOnlyDigits_Success() {
    postDistanceAndValidateRequestStatusCodeBeCreated(
        DistanceDto.builder().city1("Start").city2("222").distance(4.0).build());
  }

  @Test
  public void createDistance_PropertyStartWithMixedLettersAndDigits_Success() {
    postDistanceAndValidateRequestStatusCodeBeCreated(
        DistanceDto.builder().city1("S2tart11").city2("Finish").distance(4.0).build());
  }

  @Test
  public void createDistance_PropertyFinishWithMixedLettersAndDigits_Success() {
    postDistanceAndValidateRequestStatusCodeBeCreated(
        DistanceDto.builder().city1("Start").city2("Fiasd223nish").distance(4.0).build());
  }

  @Test
  public void rewriteDistance_SameOrderCityNames_Success() {
    // given
    testDBHelper.insert(DistanceDto.builder().city1("Start").city2("Finish").distance(1.0).build());
    DistanceDto expected = DistanceDto.builder().city1("Start").city2("Finish").distance(4.0).build();

    // when
    postDistanceAndValidateRequestStatusCodeBeCreated(expected);

    // then
    List<DistanceDto> distanceDtos = testDBHelper.getAll();
    Assert.assertEquals(expected, distanceDtos.get(0));
    Assert.assertEquals(1, distanceDtos.size());
  }

  @Test
  public void rewriteDistance_NotSameOrderCityNames_Success() {
    // given
    testDBHelper.insert(DistanceDto.builder().city1("Start").city2("Finish").distance(1.0).build());
    DistanceDto expected = DistanceDto.builder().city1("Finish").city2("Start").distance(4.0).build();

    // when
    postDistanceAndValidateRequestStatusCodeBeCreated(expected);

    // then

    // NOTE!!!
    // Only distance changed, not cities order.
    expected = DistanceDto.builder().city1("Start").city2("Finish").distance(4.0).build();
    List<DistanceDto> distanceDtos = testDBHelper.getAll();
    Assert.assertEquals(expected, distanceDtos.get(0));
    Assert.assertEquals(1, distanceDtos.size());
  }
}
