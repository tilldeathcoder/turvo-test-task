package com.turvo.configuration;

import com.turvo.domain.City;
import com.turvo.domain.Distance;
import com.turvo.repository.DistanceRepository;
import com.turvo.service.dto.DistanceDto;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Component
@Profile("dev")
public class TestDBHelper {

  private SessionFactory sessionFactory;
  private DistanceRepository distanceRepository;

  @Autowired
  public TestDBHelper(SessionFactory sessionFactory, DistanceRepository distanceRepository) {
    this.sessionFactory = sessionFactory;
    this.distanceRepository = distanceRepository;
  }

  @Transactional
  public void cleanDB() {
    Session session = sessionFactory.getCurrentSession();
    session.createSQLQuery("SET REFERENTIAL_INTEGRITY FALSE").executeUpdate();
    List<String> tables =
        session
            .createSQLQuery(
                "SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES  where TABLE_SCHEMA='PUBLIC'")
            .getResultList();
    tables.forEach(table -> session.createNativeQuery("TRUNCATE TABLE " + table).executeUpdate());

    List<String> sequences =
        session
            .createNativeQuery(
                "SELECT SEQUENCE_NAME FROM INFORMATION_SCHEMA.SEQUENCES WHERE SEQUENCE_SCHEMA='PUBLIC'")
            .getResultList();
    sequences.forEach(
        sequence -> session.createNativeQuery("ALTER SEQUENCE " + sequence + " RESTART WITH 1"));
    session.createNativeQuery("SET REFERENTIAL_INTEGRITY TRUE").executeUpdate();
  }

  @Transactional
  public List<DistanceDto> getAll() {
    Session session = this.sessionFactory.getCurrentSession();
    List<Distance> distances = session.createQuery("from Distance").list();

    return distances.stream().map(DistanceDto::new).collect(Collectors.toList());
  }

  @Transactional
  public void insert(DistanceDto... distances) {
    Arrays.stream(distances)
        .forEach(
            distance ->
                distanceRepository.insert(
                    Distance.builder()
                        .city1(City.builder().title(distance.getCity1()).build())
                        .city2(City.builder().title(distance.getCity2()).build())
                        .distance(distance.getDistance())
                        .build()));
  }
}
